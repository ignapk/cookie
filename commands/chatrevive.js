exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
	/*var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200)
			message.channel.send(this.responseText.substring(39));
	}
	xhr.open("POST", "https://www.conversationstarters.com/random.php", true);
	xhr.send();*/
	var fs=require('fs');
	list = fs.readFileSync('list.txt').toString().split("\n");
	let index = Math.floor(Math.random()*(list.length-1));
	message.channel.send(list[index]);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["chat","chatrevuve","chatreviev","chatrevige","chatreive","chatrevibe","chatrevivr","chetrcuce","chatrcuce","dead","deadchat","daed","daedchat"],
  permLevel: "User"
};

exports.help = {
  name: "chatrevive",
  category: "Miscellaneous",
  description: "Revive chat with interesting topics",
  usage: "chatrevive"
};

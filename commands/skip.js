exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
    const queue = client.distube.getQueue(message);
    if (!queue) return message.channel.send("There is nothing in the queue right now!");
    try {
      const song = await queue.skip();
      message.channel.send(`Skipped! Now playing:\n${song.name}`);
    } catch (e) {
      message.channel.send(`Error: ${e}`);
    }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "skip",
  category: "Music",
  description: "Skips a song",
  usage: "skip"
};

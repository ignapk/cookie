exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
    const queue = client.distube.getQueue(message);
    if (!queue) return message.channel.send("There is nothing in the queue right now!");
    const song = await queue.previous();
    message.channel.send(`Now playing:\n${song.name}`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "previous",
  category: "Music",
  description: "Plays the previous song in the queue.",
  usage: "previous"
};

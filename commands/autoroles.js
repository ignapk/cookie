exports.run = (client, message, [com, ...args], level) => {
	switch(com) {
		case 'add':
			var name =args.join(' ');
			if (name.trim()=="")
			message.channel.send("Usage: autoroles <add|list|remove> [name]");
			else
			client.autoroles.push(message.member.guild.name, name);
			break;
		case 'ls':
		case 'list':
			var array = client.autoroles.get(message.member.guild.name);
			var list = "Active autoautoroles:\n";
			for (var i=0; i<array.length; i++) list+=array[i]+"\n";
			message.channel.send(list);
			break;
		case 'rm':
		case 'remove':
			var name = args.join(' ');
			if (name.trim()=="" || !client.autoroles.get(message.member.guild.name).includes(name))
			message.channel.send("Usage: autoroles <add|list|remove> [name]");
			else
			client.autoroles.remove(message.member.guild.name,name);
			break;
		default:
			message.channel.send("Usage: autoroles <add|list|remove> [name]");
	}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Administrator"
};

exports.help = {
  name: "autoroles",
  category: "Miscellaneous",
  description: "Manages various lists of automatic server autoroles",
  usage: "autoroles <add|list|remove> [name]"
};

exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
    const queue = client.distube.getQueue(message)
    if (!queue) return message.channel.send("There is nothing in the queue right now!")
    if (queue.paused) {
      queue.resume()
      message.channel.send('Resumed the song.')
    } else {
      message.channel.send('The queue is not paused!')
    }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "resume",
  category: "Music",
  description: "Resumes the queue.",
  usage: "resume"
};

exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
	const string = args.join(' ');
	if (!string) return message.channel.send("Usage: play <song name or url>");
	client.distube.play(message.member.voice.channel, string, {
	  message,
	  textChannel: message.channel,
	  member: message.member
        });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "play",
  category: "Music",
  description: "Plays a song",
  usage: "play <song name or url>"
};

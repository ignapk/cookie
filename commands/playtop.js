exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
    const string = args.join(' ');
    if (!string) return message.channel.send(`Usage: playtop <song name or url>`);
    client.distube.play(message.member.voice.channel, string, {
      member: message.member,
      textChannel: message.channel,
      message,
      position: 1
    });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "playtop",
  category: "Music",
  description: "Plays a song a the beginning of the queue",
  usage: "playtop <song name or url>"
};

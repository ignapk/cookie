exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
    const queue = client.distube.getQueue(message);
    if (!queue) return message.channel.send("There is nothing playing!");
    const qp = queue.previousSongs
	.map(song => `${song.name} - \`${song.formattedDuration}\``)
	.join('\n');
    const q = queue.songs
      .map((song, i) => `${i === 0 ? 'Playing:' : `${i}`} ${song.name} - \`${song.formattedDuration}\``)
      .join('\n');
    message.channel.send(`**Server Queue**\n${qp}\n${q}`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "queue",
  category: "Music",
  description: "Show current queue.",
  usage: "queue"
};

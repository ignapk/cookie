const logger = require('../modules/logger');
const https = require('https');
const fs = require('fs');

exports.run = (client, message, [com, ...args], level) => {
	switch(com) {
		case 'import':
			if (message.attachments.size == 0 && args.join(' ').trim()=="") {
				message.channel.send("Neither file nor url given");
				break;
			}

			message.attachments.each(attachment => args.push(attachment.url));

			if (args.length != 2) {
				message.channel.send('Needed second url/attachment');
				break;
			}

			url = args[0];
			const filename = url.split('?')[0].split('/').pop();
			const filestream = fs.createWriteStream(filename);
			https.get(url, response => {
				response.pipe(filestream);
				filestream.on('finish', () => {
					filestream.close();
					try {
						const data = fs.readFileSync(filename, 'utf8');

			url2 = args[1];
			const filename2 = url2.split('?')[0].split('/').pop();
			const filestream2 = fs.createWriteStream(filename2);
			https.get(url2, response2 => {
				response2.pipe(filestream2);
				filestream2.on('finish', () => {
					filestream2.close();
					try {
						const data2 = fs.readFileSync(filename2, 'utf8');

						filter = data.split('\n');
						list = data2
							.split('\n')
							.slice(1)
							.filter(line => {
								values = line.split(',')
								return values[6] == 1 && filter.includes(values[3] + ' ' + values[4]);
							})
							.map(line => line.split(',')[7])
							.forEach(nick => {
								message.channel.send(`Giving ${nick} role "1 Rok"`);
								message.guild.members.fetch()
								.then( members => {

								m = members.find(member => {
									return [member.user.username, member.user.tag, member.user.displayName, member.user.globalName, member.nickname, member.displayName].includes(nick);
								});
								if (!m) {
									message.channel.send(`No such user ${nick}`);
								} else if (m.roles.cache.size > 1) {
									message.channel.send(`User ${nick} already has a role`);
								} else {
									r = message.guild.roles.cache.find(role => role.name === "1 Rok");
									if (r) {
										m.roles.add(r);
										message.channel.send(`Given ${nick} role "1 Rok"`);
									} else {
										message.guild.roles.create ({name: "1 Rok", color: "#FF69B4", reason: `Role "1 Rok" didn't exist`}).then(role => {
											message.channel.send(`Role ${role.name} created`);
										m.roles.add(role);
										message.channel.send(`Given ${nick} role "1 Rok"`);
										}).catch(error => {
											logger.log(`Error creating a role: ${error}`, 'error');
										});
									}
								}

								});
							});
					} catch (error) {
						logger.log(`Error reading data: ${error}`, 'error');
						return;
					}
					fs.unlinkSync(filename2);

				});
			}).on('error', error => {
				fs.unlinkSync(filename2);
				logger.log(`Error downloading data: ${error}`, 'error');
				return;
			});
					} catch (error) {
						logger.log(`Error reading filter: ${error}`, 'error');
						return;
					}
					fs.unlinkSync(filename);

				});
			}).on('error', error => {
				fs.unlinkSync(filename);
				logger.log(`Error downloading filter: ${error}`, 'error');
				return;
			});
				/*switch (attachment.contentType) {
					case 'text/csv; charset=utf-8':
						message.channel.send(`Unknown file type: ${attachment.contentType}`);
				}*/
			break;
		default:
			message.channel.send(module.exports.help.usage);
	}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Administrator"
};

exports.help = {
  name: "roles",
  category: "Miscellaneous",
  description: "Sets roles based on different input",
  usage: "roles <import> <filter text file url or attachment> <csv file url or attachment>"
};

exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
    const queue = client.distube.getQueue(message)
    if (!queue) return message.channel.send("There is nothing in the queue right now!")
    if (queue.paused) {
      return message.channel.send('The queue is already paused.')
    }
    queue.pause()
    message.channel.send('Paused the queue.')
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "pause",
  category: "Music",
  description: "Pauses the queue.",
  usage: "pause"
};

exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars
    const queue = client.distube.getQueue(message)
    if (!queue) return message.channel.send(`There is nothing in the queue right now!`)
    queue.stop()
    message.channel.send(`Stopped!`)
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "stop",
  category: "Music",
  description: "Stops the queue.",
  usage: "stop"
};

exports.run = async (client, message, [position], level) => { // eslint-disable-line no-unused-vars
	const queue = client.distube.getQueue(message);
	if (!queue) return message.channel.send("There is nothing playing.");
	if (!position) return message.channel.send("Usage delete <number of a song in the queue>");
	queue.songs.splice(position, 1);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "delete",
  category: "Music",
  description: "Deletes a song from the queue",
  usage: "delete <number of a song in the queue>"
};

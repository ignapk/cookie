/*
Logger class for easy and aesthetically pleasing console logging 
*/
const { cyan, red, magenta, gray, yellow, white, green } = require("colorette");

exports.log = (content, type = "log") => {
  switch (type) {
    case "log": return console.log(`${gray(type.toUpperCase())} ${content} `);
    case "warn": return console.log(`${yellow(type.toUpperCase())} ${content} `);
    case "error": return console.log(`${red(type.toUpperCase())} ${content} `);
    case "debug": return console.log(`${magenta(type.toUpperCase())} ${content} `);
    case "cmd": return console.log(`${white(type.toUpperCase())} ${content}`);
    case "ready": return console.log(`${green(type.toUpperCase())} ${content}`);
    default: throw new TypeError("Logger type must be either warn, debug, log, ready, cmd or error.");
  }
}; 

exports.error = (...args) => this.log(...args, "error");

exports.warn = (...args) => this.log(...args, "warn");

exports.debug = (...args) => this.log(...args, "debug");

exports.cmd = (...args) => this.log(...args, "cmd");

const logger = require("../modules/logger.js");
module.exports = async (client, error, queue, song) => {
  logger.log(`An error event was sent by DisTube: \n${error}`, "error");
};

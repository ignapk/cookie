const logger = require("../modules/logger.js");
module.exports = async (client, queue, song) => {
	queue.textChannel.send(
      `Playing \`${song.name}\` - \`${song.formattedDuration}\``
    );
};

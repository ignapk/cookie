const logger = require("../modules/logger.js");
const { getSettings, getRandom } = require("../modules/functions.js");
module.exports = async client => {
  // Log that the bot is online.
  logger.log(`${client.user.tag}, ready to serve ${client.guilds.cache.map(g => g.memberCount).reduce((a, b) => a + b)} users in ${client.guilds.cache.size} servers.`, "ready");
  
  // Make the bot "play the game" which is the help command with default prefix.
  client.user.setActivity(`${getSettings("default").prefix}help`, { type: "PLAYING" });

  // troll
  client.troll = (client, channel, guild) => {
    let time = Math.floor(Math.random()*(86400-10800+1)+10800)*1000;
    if (getSettings(guild).trollEnabled === "true" && channel) channel.send(getRandom(client.messages.getProp(guild.name,'troll'),null));
    setTimeout(client.troll,time,client,channel,guild);
  };

  // Log connected guilds and create corresponding messages objects
  client.guilds.cache.forEach(g=>{
    logger.log(`${g.name}`, "ready");
    client.messages.ensure(g.name, {welcome: {default: "Default welcome message"}, goodbye: {default: "Default goodbye message"}, unknown: {default: "Default unknown message"}, ban:{default:"Default ban message"}, troll:{default:"Default troll message"}});
    client.roles.ensure(g.name, ["Default role name"]);
    client.responds.ensure(g.name, {"Hello there": "General Kenobi?"});
    // COOKIE: start trolling
    let time = Math.floor(Math.random()*(86400-10800+1)+10800)*1000;
    let channel = g.channels.cache.find(c => c.name === getSettings(g).trollChannel)
    setTimeout(client.troll,time,client,channel,g);
  });

  // cookie speaks and spies!
var cookieSpeak = (function* (){
                while(true){
                var rl = require('readline').createInterface({
                 input: process.stdin,
                 output: process.stdout
                });
                logger.log('Available guilds: ', "ready");
                client.guilds.cache.forEach( g => { logger.log(`${g.name}`, "ready"); });
                logger.log('Write name of the guild you want to send messages to below:', "ready");
                var answer = yield rl.question(' ', res=>cookieSpeak.next(res));
                var guild=client.guilds.cache.find(val => val.name === answer);
                if(guild==null){ logger.log('Guild not found, try again.', "ready");rl.close();continue;}
                var guildname=answer;
		client.guildname=guildname;
                logger.log('Available channels:', "ready");
                guild.channels.cache.forEach( (ch) => { logger.log(`${ch.name}`, "ready"); });
                logger.log('Write name of the channel you want to send messages to below:', "ready");
                var name = yield rl.question(' ', res=>cookieSpeak.next(res));
                var channel = guild.channels.cache.find(val => val.name === name);
                if(channel==null) {logger.log('Channel not found, try again.', "ready");rl.close();continue;}
                var channelname=name;
		client.channelname=channelname;	
                logger.log(`Send messages to ${guild.name}#${channel.name} by writing them below:`, "ready");
                var ri = yield rl.on('line', (input) => {
                        switch(input){
                        case 'exit':
                                client.spy=false;
                                cookieSpeak.next(input);return;break;
                        case 'spy':
                                client.spy=true;
                                logger.log(`Spying on ${guild.name}#${channel.name}:`, "ready");break;
                        default:
                                channel.send(input);
                        }});
                rl.close();
                }
        })();
        cookieSpeak.next();
};

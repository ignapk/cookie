const logger = require("../modules/logger.js");
const { getSettings, permlevel, getRandom } = require("../modules/functions.js");
const config = require("../config.js");

// The MESSAGE event runs anytime a message is received
// Note that due to the binding of client to every event, every event
// goes `client, other, args` when this function is run.

module.exports = async (client, message) => {
  // Grab the container from the client to reduce line length.
  const { container } = client;
  // It's good practice to ignore other bots. This also makes your bot ignore itself
  // and not get into a spam loop (we call that "botception").
  if (message.author.bot) return;

  // COOKIE: Check if dm, and then proceed to cookie speak!
  if (message.guild === null) {
    if (message.content.startsWith("guild")) {
      var answer = message.content.substring(6);
      client.targetGuild = client.guilds.cache.find(val => val.name === answer);
      if (client.targetGuild === null) {
	message.author.send("Guild not found, try again.");
	return;
      }
    } else if (message.content.startsWith("channel")) {
      var answer = message.content.substring(8);
      client.targetChannel = client.targetGuild.channels.cache.find(val => val.name === answer);
      if (client.targetChannel === null) {
	message.author.send("Channel not found, try again.");
	return;
      } else {
	message.author.send(`Send messages to ${client.targetGuild.name}#${client.targetChannel.name} by writing them below (send "stop" to well, stop):`);
	return;
      }
    } else if (message.content === "stop") {
      message.author.send("Stopped chatting.");
      client.targetGuild = null;
      client.targetChannel = null;
    }
    if (client.targetGuild === null) {
      message.author.send("Available guilds:");
      client.guilds.cache.forEach( g => { message.author.send(g.name); });
      message.author.send("Write guild=... to select a guild.");
      return;
    }
    if (client.targetChannel === null) {
      message.author.send("Available channels:");
      client.targetGuild.channels.cache.forEach( (ch) => {
	message.author.send(ch.name);
      });
      message.author.send("Write channel=... to select a channel.");
      return;
    }
    client.targetChannel.send(message.content);
    return;
  }

  // Grab the settings for this server from Enmap.
  // If there is no guild, get default conf (DMs)
  const settings = message.settings = getSettings(message.guild);

  // Checks if the bot was mentioned via regex, with no message after it,
  // returns the prefix. The reason why we used regex here instead of
  // message.mentions is because of the mention prefix later on in the
  // code, would render it useless.
  const prefixMention = new RegExp(`^<@!?${client.user.id}> ?$`);
  if (message.content.match(prefixMention)) {
    return message.reply(`My prefix on this guild is \`${settings.prefix}\``);
  }

  // It's also good practice to ignore any and all messages that do not start
  // with our prefix, or a bot mention.
  const prefix = new RegExp(`^<@!?${client.user.id}> |^\\${settings.prefix}`).exec(message.content);
  // This will return and stop the code from continuing if it's missing
  // our prefix (be it mention or from the settings).
  // COOKIE: Before stopping, check for trigger for autoresponder
  if (!prefix) {
    for (var trigger in client.responds.get(message.guild.name)) {
      if (message.content.toLowerCase().includes(trigger))
        return message.channel.send(client.responds.get(message.guild.name, trigger));
    }
    return;
  }
    
  // COOKIE: Checks if the prefix was used with a number, specific rule for $ sign
  if (message.content.match(/^\$[0-9]+/)) return;

  // COOKIE: SPY
  if (message.guild.name==client.guildname && message.channel.name==client.channelname && client.spy) console.log(message.member.user.tag+": "+message.content);

  // Here we separate our "command" name, and our "arguments" for the command.
  // e.g. if we have the message "+say Is this the real life?" , we'll get the following:
  // command = say
  // args = ["Is", "this", "the", "real", "life?"]
  const args = message.content.slice(prefix[0].length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  // If the member on a guild is invisible or not cached, fetch them.
  if (message.guild && !message.member) await message.guild.members.fetch(message.author);

  // Get the user or member's permission level from the elevation
  const level = permlevel(message);

  // Check whether the command, or alias, exist in the collections defined
  // in app.js.
  const cmd = container.commands.get(command) || container.commands.get(container.aliases.get(command));
  // using this const varName = thing OR otherThing; is a pretty efficient
  // and clean way to grab one of 2 values!
  if (!cmd) {
  // COOKIE: log unknown command usage send unknown command message
    logger.log(`${config.permLevels.find(l => l.level === level).name} ${message.author.id} tried to run command ${command}`, "cmd");
    return message.channel.send(getRandom(client.messages.getProp(message.member.guild.name,'unknown'),message.member));
  }
  // Some commands may not be useable in DMs. This check prevents those commands from running
  // and return a friendly error message.
  if (cmd && !message.guild && cmd.conf.guildOnly)
    return message.channel.send("This command is unavailable via private message. Please run this command in a guild.");

  if (!cmd.conf.enabled) return;

  if (level < container.levelCache[cmd.conf.permLevel]) {
    if (settings.systemNotice === "true") {
      return message.channel.send(`You do not have permission to use this command.
Your permission level is ${level} (${config.permLevels.find(l => l.level === level).name})
This command requires level ${container.levelCache[cmd.conf.permLevel]} (${cmd.conf.permLevel})`);
    } else {
      return;
    }
  }

  // To simplify message arguments, the author's level is now put on level (not member so it is supported in DMs)
  // The "level" command module argument will be deprecated in the future.
  message.author.permLevel = level;
  
  message.flags = [];
  while (args[0] && args[0][0] === "-") {
    message.flags.push(args.shift().slice(1));
  }
  // If the command exists, **AND** the user has permission, run it.
  try {
    await cmd.run(client, message, args, level);
    logger.log(`${config.permLevels.find(l => l.level === level).name} ${message.author.id} ran command ${cmd.help.name}`, "cmd");
  } catch (e) {
    console.error(e);
    message.channel.send({ content: `There was a problem with your request.\n\`\`\`${e.message}\`\`\`` })
      .catch(e => console.error("An error occurred replying on an error", e));
  }
};

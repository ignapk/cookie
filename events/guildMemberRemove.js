const { getSettings, getRandom } = require("../modules/functions.js");

module.exports = (client, member) => {
  const settings = getSettings(member.guild);
  if (settings.welcomeEnabled !== "true") return;
  // get the general channel
  var channel = member.guild.channels.cache.find(val => val.name === 'general');
  if(!channel) channel = member.guild.channels.cache.at(1);
  // Send the goodbye message to the welcome channel
  member.guild.bans.fetch().then(bans =>{
    if(bans.has(member.user.id))
      channel.send(getRandom(client.messages.getProp(member.guild.name,'ban'),member));
    else if (member.roles.cache.some(r=>["Shield Recruit"].includes(r.name)))
      channel.send(member.user.tag+' has escaped, 2500 renown for the person who brings them back alive!');
    else
      channel.send(getRandom(client.messages.getProp(member.guild.name,'goodbye'),member));
  });
};
